# TP2 pt. 1 : Gestion de service
## 0. Prérequis
## I. Un premier serveur web
### 1. Installation
- Installer le serveur Apache
```
[kalop@web ~]$ sudo dnf install httpd
```
- Démarrer le service Apache
```
[kalop@web conf]$ sudo systemctl start httpd
```

```
[kalop@web conf]$ sudo nano /etc/systemd/system/httpd.service
[kalop@web conf]$ [kalop@web conf]$ sudo systemctl daemon-reload
[kalop@web conf]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /etc/systemd/system/httpd.service.
[kalop@web conf]$ sudo systemctl start httpd
[kalop@web conf]$ sudo systemctl status httpd
● httpd.service
   Loaded: loaded (/etc/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 12:29:05 CEST; 3min 40s ago
 Main PID: 24012 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─24012 /usr/sbin/httpd -DFOREGROUND
           ├─24013 /usr/sbin/httpd -DFOREGROUND
           ├─24014 /usr/sbin/httpd -DFOREGROUND
           ├─24015 /usr/sbin/httpd -DFOREGROUND
           └─24016 /usr/sbin/httpd -DFOREGROUND

Sep 29 12:31:56 web.tp2.linux systemd[1]: /etc/systemd/system/httpd.service:4: Assignment outside of section. Ignoring.
Sep 29 12:31:56 web.tp2.linux systemd[1]: httpd.service: Current command vanished from the unit file, execution of the >
Sep 29 12:32:04 web.tp2.linux systemd[1]: httpd.service: Got notification message from PID 24012, but reception is disa>
Sep 29 12:32:05 web.tp2.linux systemd[1]: /etc/systemd/system/httpd.service:1: Assignment outside of section. Ignoring.
Sep 29 12:32:05 web.tp2.linux systemd[1]: /etc/systemd/system/httpd.service:3: Assignment outside of section. Ignoring.
Sep 29 12:32:05 web.tp2.linux systemd[1]: /etc/systemd/system/httpd.service:4: Assignment outside of section. Ignoring.
Sep 29 12:32:14 web.tp2.linux systemd[1]: httpd.service: Got notification message from PID 24012, but reception is disa>
Sep 29 12:32:24 web.tp2.linux systemd[1]: httpd.service: Got notification message from PID 24012, but reception is disa>
Sep 29 12:32:34 web.tp2.linux systemd[1]: httpd.service: Got notification message from PID 24012, but reception is disa>
Sep 29 12:32:44 web.tp2.linux systemd[1]: httpd.service: Got notification message from PID 24012, but reception is disa>
lines 1-24/24 (END)
```
- TEST
```
[kalop@web conf]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
```

```
PS C:\Users\rukab> curl 10.102.1.11
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
```

```
[kalop@web conf]$ sudo systemctl status httpd
● httpd.service
   Loaded: loaded (/etc/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 12:29:05 CEST; 11min ago
 Main PID: 24012 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 25.5M
```

### 2. Avancer vers la maîtrise du service
-  Le service Apache...
```
sudo systemctl enable httpd
```
```
sudo systemctl is-enabled httpd
```
```
[kalop@web ~]$ cat /etc/httpd/conf/httpd.conf
```
-  Déterminer sous quel utilisateur tourne le processus Apache
```
[kalop@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
```


```
[kalop@web ~]$ ps -ef
...
apache     14216   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     14217   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     14218   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     14219   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
...
```

```
[kalop@web www]$ ls -la
total 4
drwxr-xr-x.  4 root root   33 Oct  6 09:42 .
drwxr-xr-x. 22 root root 4096 Oct  6 09:42 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```
- Changer l'utilisateur utilisé par Apache
```
[kalop@web ~]$ ps -ef
...
testweb    14216   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
testweb    14217   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
testweb    14218   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
testweb    14219   14215  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
...
```
-  Faites en sorte que Apache tourne sur un autre port
```
[kalop@web ~]$ sudo ss -l -p -n -t
1:34]
State                    Recv-Q                   Send-Q                                       Local Address:Port                                       Peer Address:Port                   Process
LISTEN                   0                        128                                                0.0.0.0:22                                              0.0.0.0:*                       users:(("sshd",pid=834,fd=5))
LISTEN                   0                        128                                                   [::]:22                                                 [::]:*                       users:(("sshd",pid=834,fd=7))
LISTEN                   0                        128                                                      *:81                                                    *:*                       users:(("httpd",pid=14831,fd=4),("httpd",pid=148
```

```
C:\Users\rukab> curl http://10.102.1.11:81
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page
you've expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the
website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
````

### II. Une stack web plus avancée
#### 1. Intro
#### 2. Setup
- A. Serveur Web et NextCloud
```
[kalop@web config]$ history
.........
   43  sudo dnf install epel-release -y
   44  sudo dnf update -y
   45  sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
   46  sudo dnf module list php
   47  sudo dnf module enable php:remi-7.4
   48  sudo dnf module list php
   49  sudodnf install vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
   50  sudo dnf install vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
   51  sudo systemctl is-enabled httpd
   52  sudo vim /etc/httpd/conf/httpd.conf
   53  vim /etc/httpd/sites-available/com.yourdomain.nextcloud
   54  sudo vim /etc/httpd/sites-available/com.yourdomain.nextcloud
   55  cat /etc/httpd/sites-available/com.yourdomain.nextcloud
   56  vi /etc/httpd/sites-available/com.yourdomain.nextcloud
   57  sudo vi /etc/httpd/sites-available/com.yourdomain.nextcloud
   58  sudo dnf install php
   59  sudo dnf module list php
   60  mkdir /etc/httpd/sites-available
   61  sudo mkdir /etc/httpd/sites-available
   62  sudo mkdir /etc/httpd/sites-enabled
   63  sudo mkdir /var/www/sub-domains/
   64  sudo vim /etc/httpd/sites-available/com.yourdomain.nextcloud
   65  sudo ln -s /etc/httpd/sites-available/com.yourdomain.nextcloud /etc/httpd/sites-enabled/
   66  sudo mkdir -p /var/www/sub-domains/com.yourdomain.com/html
   67  timedatectl
   68  vim /etc/opt/remi/php74/php.ini
   69  sudo vim /etc/opt/remi/php74/php.ini
   70  pwd
   71  wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
   72  unzip nextcloud-22.2.0.zip
   73  ls
   74  rm -r  nextcloud
   75  rm nextcloud-22.2.0.zip
   76  ls
   77  wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
   78  unzip nextcloud-21.0.1.zip
   79  cd nextcloud/
   80  cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
   81  sudo cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
   82  cd /var/www/sub-domains/com.yourdomain.com/html/
   83  ls
   84  cd ..
   85  ls
   86  cd ..
   87  ls
   88  rm -r sub-domains/
   89  ls
   90  sudo rm -r sub-domains/
   91  ls
   92  cd
   93  pwd
   94  mkdir -p /var/www/sub-domains/com.yourdomain.com.nextcloud/html
   95  sudo mkdir -p /var/www/sub-domains/com.yourdomain.com.nextcloud/html
   96  ls
   97  cd nextcloud/
   98  cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
   99  cd /var/www/sub-domains/com.yourdomain.com.nextcloud/html
  100  ls
  101  pwd
  102  sudo cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
  103  cd
  104  cd nextcloud/
  105  sudo cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
  106  cd /var/www/
  107  ls
  108  sudo rm -r sub-domains/
  109  ls
  110  cd
  111  cd nextcloud/
  112  sudo mkdir -p /var/www/sub-domains/com.yourdomain.nextcloud/html/
  113  sudo cp -Rf * /var/www/sub-domains/com.yourdomain.nextcloud/html/
  114  chown -Rf apache.apache /var/www/sub-domains/com.yourdomain.nextcloud/html
  115  cd /var/www/sub-domains/com.yourdomain.nextcloud/html/
  116  ls
  117  ls -la
  118  mv /var/www/sub-domains/com.yourdomain.nextcloud/html/data /var/www/sub-domains/com.yourdomain.nextcloud/
  119  ls
  120  mkdir data
  121  sudo mkdir data
  122  ls
  123  ls -la
  124  chmod 755 data
  125  sudo chmod 755 data/
  126  cd
  127  mv /var/www/sub-domains/com.yourdomain.nextcloud/html/data /var/www/sub-domains/com.yourdomain.nextcloud/
  128  sudo mv /var/www/sub-domains/com.yourdomain.nextcloud/html/data /var/www/sub-domains/com.yourdomain.nextcloud/
  129  sudo systemctl restart httpd
  130  cd /var/www/sub-domains/com.yourdomain.nextcloud/
  131  ls
  132  cd html/
  133  ls
  134  find config.php
  135  cd config/
  136  ls
  137  cd ..
  138  cd apps/
  139  ls
  140  ls -la
  141  cd logreader/
  142  ls
  143  cd ..
  144  ls
  145  cd ..
  146  cd config/
  147  ls
  148  cat CAN_INSTALL
  149  ls -la
  150  cat config.sample.php
  151  clear
  152  ls
  153  cd ..
  154  ls
  155  sudo journalctl -xe -u httpd
  156  sudo cat /var/log/httpd/acces_log
  157  sudo cat /var/log/httpd/error_log
  158  clear
  159  sudo cat /var/log/httpd/error_log
  160  sudo cat /var/log/httpd/access_log
  161  sudo cat /var/log/httpd/
  162  cd /var/log/httpd/
  163  sudo cd /var/log/httpd/
  164  lq
  165  pwd
  166  sudo cd /var/log/httpd/
  167  ls
  168  sudo su
  169  sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8
  170  clear
  171  sudo nmcli con realod
  172  sudo nmcli con reload
  173  sudo nmcli con up enp0s8
  174  ip a
  175  sudo systemctl status https
  176  sudo systemctl status httpd
  177  sudo systemctl restart httpd
  178  sudo systemctl status httpd
  179  clear
  180  cat /etc/httpd/conf/httpd.conf
  181  cd /var/www/sub-domains/
  182  ls
  183  cd com.yourdomain.nextcloud/
  184  ls
  185  cd html/
  186  ls
  187  ls -l
  188  cd ..
  189  ls -l
  190  ls
  191  cd ..
  192  ls
  193  sudo chown apache com.yourdomain.nextcloud/
  194  ls -l
  195  cd com.yourdomain.nextcloud/
  196  ls
  197  ls -l
  198  cd ..
  199  sudo chown -R apache com.yourdomain.nextcloud/
  200  ls -l
  .......
  ```
  
  
  - B. Base de données
  
  ```
  [kalop@db ~]$ history
   35  sudo dnf install mariadb-server
   36  sudo systemctl enable mariadb
   37  sudo systemctl start mariadb
   38  sudo systemctl status mariadb
   39  sudo mysql -u root
   40  ip a
   41  sudo firewall-cmd --add-port=3306/tcp --permanent
   42  sudo firewall-cmd --reload
   43  ip a
   44  history
[kalop@db ~]$
```
- Exploration de la base de données

```
[kalop@web sites-available]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 489
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authtoken                |
| oc_bruteforce_attempts      |
| oc_calendar_invitations     |
| oc_calendar_reminders       |
| oc_calendar_resources       |
| oc_calendar_resources_md    |
| oc_calendar_rooms           |
| oc_calendar_rooms_md        |
| oc_calendarchanges          |
| oc_calendarobjects          |
| oc_calendarobjects_props    |
| oc_calendars                |
| oc_calendarsubscriptions    |
| oc_cards                    |
| oc_cards_properties         |
| oc_collres_accesscache      |
| oc_collres_collections      |
| oc_collres_resources        |
| oc_comments                 |
| oc_comments_read_markers    |
| oc_dav_cal_proxy            |
| oc_dav_shares               |
| oc_direct_edit              |
| oc_directlink               |
| oc_federated_reshares       |
| oc_file_locks               |
| oc_filecache                |
| oc_filecache_extended       |
| oc_files_trash              |
| oc_flow_checks              |
| oc_flow_operations          |
| oc_flow_operations_scope    |
| oc_group_admin              |
| oc_group_user               |
| oc_groups                   |
| oc_jobs                     |
| oc_known_users              |
| oc_login_flow_v2            |
| oc_migrations               |
| oc_mimetypes                |
| oc_mounts                   |
| oc_notifications            |
| oc_notifications_pushtokens |
| oc_oauth2_access_tokens     |
| oc_oauth2_clients           |
| oc_preferences              |
| oc_privacy_admins           |
| oc_properties               |
| oc_recent_contact           |
| oc_schedulingobjects        |
| oc_share                    |
| oc_share_external           |
| oc_storages                 |
| oc_storages_credentials     |
| oc_systemtag                |
| oc_systemtag_group          |
| oc_systemtag_object_mapping |
| oc_text_documents           |
| oc_text_sessions            |
| oc_text_steps               |
| oc_trusted_servers          |
| oc_twofactor_backupcodes    |
| oc_twofactor_providers      |
| oc_user_status              |
| oc_user_transfer_owner      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
77 rows in set (0.001 sec)

MariaDB [nextcloud]>

```
- 77 tables on donc était crées après l'installation terminée